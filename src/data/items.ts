import { Item } from '../types/Item';

export const items: Item[] = [
    {
        date: new Date(2021, 9, 15),
        category: 'food',
        title: 'McDOnalds',
        value: 32.12
    },
    {
        date: new Date(2021, 9, 21),
        category: 'food',
        title: 'Burger King',
        value: 24.89
    },
    {
        date: new Date(2021, 10, 4),
        category: 'rent',
        title: 'Aluguel apto',
        value: 250.00
    },
    {
        date: new Date(2021, 10, 14),
        category: 'salary',
        title: 'Salário estágio SIASP',
        value: 450.00
    }
];